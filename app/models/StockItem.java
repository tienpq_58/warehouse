package models;

/**
 * Created by phamq_000 on 3/27/2015.
 */
public class StockItem {
    public Warehouse warehouse;           // trường quan hệ nối với Warehouse
    public Product product;               // trường quan hệ nối với Product
    public Long quantity;

    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}
