package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phamq_000 on 3/27/2015.
 */
public class Warehouse {
    public String name;
    public List<StockItem> stock = new ArrayList();  // trường quan hệ

    public String toString() {
        return name;
    }
}
